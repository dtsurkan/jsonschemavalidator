//
//  NSArray+jsonTypes.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/23/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "NSArray+jsonTypes.h"
#import "NSObject+jsonTypes.h"
#import "SVJsonSchema.h"

@implementation NSArray (jsonTypes)

-(BOOL)isJsonArray
{
    return YES;
}

+(SVType *)jsonSchema
{
    return [SVType array];
}

-(SVType *)jsonSchema
{
    SVArray* array = (SVArray*)[self.class jsonSchema];
    if ( self.count == 1 )
    {
        id item = [self lastObject];
        if ( [item isKindOfClass:[SVType class]] )
        {
            array.items = item;
        }
        else
        {
            array.items = [item jsonSchema];
        }
    }
    else if ( self.count )
    {
        NSMutableArray* schemas = [NSMutableArray array];
        for ( id obj in self)
        {
            if ( [obj isKindOfClass:[SVType class]] )
            {
                [schemas addObject:obj];
            }
            else
            {
                id schema = [obj jsonSchema];
                [schemas addObject:schema];
            }
        }
        array.items = [NSArray arrayWithArray:schemas];
    }
    
    return array;
}

@end