//
//  NSNumber+typeChecks.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 5/22/13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "NSNumber+typeChecks.h"
#import "SVJsonSchema.h"
#import "NSObject+jsonTypes.h"

@implementation NSNumber (typeChecks)

+(SVType*)jsonSchema
{
    return [SVType number];
}

-(SVType *)jsonSchema
{
    if ( [self isJsonInteger] )
    {
        return [SVType integer];
    }
    else
    {
        return [SVType number];
    }
}

-(BOOL)isJsonInteger
{
    return
    (    strcmp([self objCType], @encode(int)) == 0
     ||  strcmp([self objCType], @encode(short)) == 0
     ||  strcmp([self objCType], @encode(long)) == 0
     ||  strcmp([self objCType], @encode(long long)) == 0
     
     ||  strcmp([self objCType], @encode(unsigned char)) == 0
     ||  strcmp([self objCType], @encode(unsigned int)) == 0
     ||  strcmp([self objCType], @encode(unsigned short)) == 0
     ||  strcmp([self objCType], @encode(unsigned long)) == 0
     ||  strcmp([self objCType], @encode(unsigned long long)) == 0
     ) || [self isJsonBoolean];
}

-(BOOL)isJsonNumber
{
    return [self isJsonInteger] || ( strcmp([self objCType], @encode(float)) == 0 || strcmp([self objCType], @encode(double)) == 0 );
}

-(BOOL)isJsonBoolean
{
    return strcmp([self objCType], @encode(char)) == 0;
}

@end
